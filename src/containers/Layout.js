import React, { useState } from 'react';
import Navbar from '../components/Navbar';
import Footer from '../components/Footer';
import '../styles/Layout.css';

const Layout = ({ children }) => {
  const [menuOpen, setMenuOpen] = useState(false);
  const handleMenuToggle = () => {
    setMenuOpen(!menuOpen);
  };

  return (
    <div className='Layout'>
      <Navbar onMenuToggle={handleMenuToggle} />
      <div className='main-content'>
        {children}
      </div>
      <Footer />
    </div>
  );
}

export default Layout;
