import React, { useState } from 'react';
import '../styles/IniciarSesion.css';
import logoApp from '../assets/logoApp1.png';
import { app } from '../api/firebaseConfig';
import { getAuth, signInWithEmailAndPassword } from 'firebase/auth';
import Alert from '../components/Alert';

const IniciarSesion = () => {
  const [email, setEmail] = useState("");
  const [pass, setPass] = useState("");
  const [alerta, setAlerta] = useState(null); // Estado para gestionar la alerta

  const auth = getAuth(app);

  const mostrarAlerta = (titulo, mensaje, tipo) => {
    setAlerta({ titulo, mensaje, tipo });
  };

  const cerrarAlerta = () => {
    setAlerta(null);
  };

  const handleSubmit = (event) => {
    event.preventDefault();

    signInWithEmailAndPassword(auth, email, pass)
      .then((userCredential) => {
        mostrarAlerta('Éxito', '¡Bienvenido!', 'success');
        window.location.href = '/terratrak/Welcome';
      })
      .catch((error) => {
        mostrarAlerta('Error', 'Hubo un problema al iniciar sesión. Por favor, verifica tus credenciales.', 'error');
      });
  }

  return (
    <div className="inicio-sesion-container">
      <div className="imagen-container">
        <img src={logoApp} alt="Descripción de la imagen" className="logo" />
      </div>
      <div className="formulario-container">
        <h2>Iniciar Sesión</h2>
        <form>
          <label htmlFor="correo">Correo electrónico:</label>
          <input type="email" id="correo" name="correo" onChange={(e) => setEmail(e.target.value)} />

          <label htmlFor="contrasenia">Contraseña:</label>
          <input type="password" id="contrasenia" name="contrasenia" onChange={(e) => setPass(e.target.value)} />

          <div className="botones-container">
            <button type="button" className="iniciar-sesion-button" onClick={handleSubmit} >Iniciar Sesión</button>
          </div>
        </form>
      </div>

      {/* Mostrar la alerta si existe */}
      {alerta && (
        <Alert
          titulo={alerta.titulo}
          mensaje={alerta.mensaje}
          tipo={alerta.tipo}
          onClose={cerrarAlerta}
        />
      )}
    </div>
  );
};

export default IniciarSesion;
