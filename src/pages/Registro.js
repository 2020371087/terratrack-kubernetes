import React, { useState } from 'react';
import '../styles/Registro.css';
import logoApp from '../assets/logoApp1.png';
import { app } from '../api/firebaseConfig';
import { getAuth, createUserWithEmailAndPassword } from 'firebase/auth'
import { getFirestore, doc, setDoc } from 'firebase/firestore';

const Registro = () => {

  const [nombre, setNombre] = useState("");
  const [apellidos, setApellidos] = useState("");
  const [telefono, setTelefono] = useState(null);
  const [pass, setPass] = useState("");
  const [email, setEmail] = useState("");

  const auth = getAuth(app);
  const db = getFirestore(app);

  const handleSubmit = (event) => {
    event.preventDefault();

    createUserWithEmailAndPassword(auth, email, pass)
      .then(async (result) => {
        if (result.user) {

          const uid = auth.currentUser.uid;
          const usuario = {
            uid: uid,
            nombre: nombre,
            apellidos: apellidos,
            telefono: telefono
          };

          await setDoc(doc(db, "users", uid), usuario);

          alert('Usuario Registrado')
          window.location.href = "/";
        }
      })
      .catch((error) => {
        console.error(error)
      })
  }

  return (
    <div className="registro-container">
      <div className="imagen-container">
        <img src={logoApp} alt="Descripción de la imagen" className="logo" />
      </div>
      <div className="formulario-container">
        <h2>Registro</h2>
        <p>Registra tus datos para ser parte de este gran sitio y obtener todos sus beneficios.</p>
        <form >
          <label htmlFor="nombre">Nombre:</label>
          <input type="text" id="nombre" name="nombre" onChange={(e) => setNombre(e.target.value)} />

          <label htmlFor="nombre">Apellidos:</label>
          <input type="text" id="apellidos" name="apellidos" onChange={(e) => setApellidos(e.target.value)} />

          <label htmlFor="telefono">Teléfono:</label>
          <input type="text" id="telefono" name="telefono" onChange={(e) => setTelefono(e.target.value)} />

          <label htmlFor="correo">Correo:</label>
          <input type="email" id="correo" name="correo" onChange={(e) => setEmail(e.target.value)} />

          <label htmlFor="contrasenia">Contraseña:</label>
          <input type="password" id="contrasenia" name="contrasenia" onChange={(e) => setPass(e.target.value)} />

          <div className="botones-container">
            <button type="button" className="registro-button" onClick={handleSubmit} >Registrarse</button>
            <button type="button" className="iniciar-sesion-button">Iniciar Sesión</button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default Registro;