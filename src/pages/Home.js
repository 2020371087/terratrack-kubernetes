// Home.js
import React, { useEffect, useState } from 'react';
import TerrenoCard from '../components/TerrenoCard';
import '../styles/Home.css';
import { getFirestore, collection, getDocs, query, where, onSnapshot } from 'firebase/firestore';
import { app } from '../api/firebaseConfig';
import { Link } from 'react-router-dom';
import NoResults from '../components/NoResults';

const Home = ({ searchTerm, user }) => {

  const [terrenos, setTerrenos] = useState([]);
  const [resultados, setResultados] = useState([]);

  useEffect(() => {
    const fetchTerrenos = async () => {
      const db = getFirestore(app);
      const terrenosCollection = collection(db, 'terrenos');
      const q = query(terrenosCollection, where('activo', '==', true));
      
      const unsubscribeSnapshot = onSnapshot(q, (snapshot) => {
        const terrenosData = snapshot.docs.map((doc) => ({ id: doc.id, ...doc.data() }));
        setTerrenos(terrenosData);
      });

      return () => {
        unsubscribeSnapshot();
      }
    };

    fetchTerrenos();

  }, [])

  useEffect(() => {

    const filteredTerrenos = terrenos.filter((terreno) =>
    (!searchTerm || 
      (
        terreno.nombreTerreno && terreno.nombreTerreno.toLowerCase().includes(searchTerm.toLowerCase()) ||
        terreno.descripcion && terreno.descripcion.toLowerCase().includes(searchTerm.toLowerCase()) ||
        terreno.direccion && terreno.direccion.toLowerCase().includes(searchTerm.toLowerCase()) ||
        terreno.notasAdicionales && terreno.notasAdicionales.toLowerCase().includes(searchTerm.toLowerCase()) ||
        (terreno.precioTerreno && parseFloat(terreno.precioTerreno) <= parseFloat(searchTerm)) ||
        terreno.servicios && terreno.servicios.toLowerCase().includes(searchTerm.toLowerCase()) 
      )
    )
  );
  

    setResultados(filteredTerrenos)

  }, [searchTerm, terrenos])

  return (
    <section className='main-container-home'>
      {/* <h1 className='title-home'>Lista de Terrenos</h1> */}
      {resultados.length === 0 && (
         <NoResults message="No se encontraron resultados. Intente con otra búsqueda." />
      )}
      <div className='cards-container-home'>
        {resultados.map((terreno) => (
          <Link to={`/terratrak/DetalleTerreno/${terreno.id}`} key={terreno.id} >
            <TerrenoCard key={terreno.id} {...terreno} className='terreno-card' user={user} />
          </Link>
        ))}
      </div>
    </section>
  );
};

export default Home;