import React, { useState, useEffect } from 'react';
import '../styles/PublicarTerreno.css'
import { useNavigate, useParams } from 'react-router-dom';
import { app } from '../api/firebaseConfig';
import { getFirestore, collection, addDoc, doc, getDoc, updateDoc } from 'firebase/firestore';
import { getAuth } from 'firebase/auth';
import { getStorage, ref, uploadBytes, getDownloadURL, deleteObject } from 'firebase/storage'
import Alert from '../components/Alert';

const EditarTerreno = () => {

  const [alerta, setAlerta] = useState(null);

  const [locationAddress, setLocationAddress] = useState('');
  const [coordinates, setCoordinates] = useState({ lat: 20.65317, lng: -100.00678 });
  const [nombreTerreno, setNombreTerreno] = useState('');
  const [areaTotal, setAreaTotal] = useState('');
  const [servicios, setServicios] = useState('');
  const [descripcion, setDescripcion] = useState('');
  const [escrituras, setEscrituras] = useState(false);
  const [tituloPropiedad, setTituloPropiedad] = useState(false);
  const [valorTerreno, setValorTerreno] = useState('');
  const [observaciones, setObservaciones] = useState('');

  const db = getFirestore(app);
  const auth = getAuth(app);

  const [terreno, setTerreno] = useState({});
  const { id } = useParams();
  

  // Funcion para obtener los datos del terreno
  useEffect(() => {
    
    const fetchTerreno = async () => {
      const db = getFirestore(app);
      const docRef = doc(db, 'terrenos', id);
      const docSnap = await getDoc(docRef);

      if (docSnap.exists()) {

        const terrenoData = docSnap.data();
        setTerreno(terrenoData || {});

        setNombreTerreno(terrenoData.nombreTerreno || '');
        setAreaTotal(terrenoData.areaTotal || '');
        setLocationAddress(terrenoData.direccion || '')
        setServicios(terrenoData.servicios || '');
        setDescripcion(terrenoData.descripcion || '');
        setValorTerreno(terrenoData.precioTerreno || '');
        setObservaciones(terrenoData.notasAdicionales || '');
        setEscrituras(terrenoData.escrituras || false);
        setTituloPropiedad(terrenoData.tituloPropiedad || false);
        console.log(terreno)
      } else {
        alert('Ocurrio un error');
        window.location.href = '/terratrak/Welcome'
      }
    }

    fetchTerreno();
  }, [id])

  const navigate = useNavigate();

  const mostrarAlerta = (titulo, mensaje, tipo) => {
    setAlerta({ titulo, mensaje, tipo });
  };

  const cerrarAlerta = () => {
    setAlerta(null);
  };

  const navigateToWelcome = () => {
    navigate('/terratrak/Welcome');
  };

  let uid;
  auth.onAuthStateChanged((user) => {
    if (user) {
      uid = user.uid
    }
  });

  const uploadPhoto = async (file, existingURL) => {
    const storage = getStorage(app);
    
    // si existe una URL existente, elimina la imagen anterior
    if (existingURL) {
      const existingImageRef = ref(storage, existingURL);
      await deleteObject(existingImageRef);
    }

    const storageRef = ref(storage, `terrenos/${uid}/${file.name}`);
    await uploadBytes(storageRef, file);
    const photoURL = await getDownloadURL(storageRef);
    return photoURL;
  }

  const handleFormSubmit = async (e) => {
    e.preventDefault();

    // Obtener foto del formulario
    const fotoInput = document.getElementById('foto');
    const fotoFile = fotoInput.files[0];

    const formData = {
      nombreTerreno: nombreTerreno,
      urlFoto: terreno && terreno.urlFoto, // Utiliza la URL existente si hay una
      direccion: locationAddress,
      coordenadas: coordinates,
      areaTotal: areaTotal,
      servicios: servicios,
      descripcion: descripcion,
      escrituras: escrituras,
      tituloPropiedad: tituloPropiedad,
      precioTerreno: valorTerreno,
      notasAdicionales: observaciones,
      user_uid: uid
    };

    if (fotoFile) {
      const photoURL = await uploadPhoto(fotoFile, terreno && terreno.urlFoto);
      formData.urlFoto = photoURL;
    }
    
    // Si ya existe el terreno, actualiza el documento, de lo contrario, agrega uno nuevo
    if (terreno) {
      const terrenoRef = doc(db, 'terrenos', id);
      await updateDoc(terrenoRef, formData);
    } else {
      await addDoc(collection(db, "terrenos"), formData);
    }

    mostrarAlerta('Éxito', 'Terreno publicado exitosamente!', 'success')
    navigateToWelcome()
   
  };

  const handleEscriturasChange = (e) => {
    setEscrituras(e.target.checked);
  }

  const handleTituloPropiedadChange = (e) => {
    setTituloPropiedad(e.target.checked);
  }

  useEffect(() => {

    if (terreno && terreno.coordenadas) {

      const coordenadas = terreno.coordenadas;

      if (typeof coordenadas === 'object' && coordenadas.lat && coordenadas.lng) {
        setCoordinates(coordenadas);
      } else if (typeof coordenadas === 'string') {
        const coordenadasArray = coordenadas
          .replace('(', '')
          .replace(')', '')
          .split(',')
          .map(coord => parseFloat(coord.trim()));

        const terrenoCoords = {
          lat: coordenadasArray[0],
          lng: coordenadasArray[1]
        };

        setCoordinates(terrenoCoords);
      }

      //const terrenoCoords = JSON.parse(terreno.coordenadas); // Convierte la cadena de coordenadas a objeto
      

      const map = new window.google.maps.Map(document.getElementById('map'), {
        center: coordinates,
        zoom: 15
      });

      const tempMarker = new window.google.maps.Marker({
        position: coordinates,
        map,
        draggable: true
      });
    }

    initMap();
  }, [terreno]);

  const initMap = () => {

    const map = new window.google.maps.Map(document.getElementById('map'), {
      center: coordinates,
      zoom: 15,
    })

    const geocoder = new window.google.maps.Geocoder();

    const tempMarker = new window.google.maps.Marker({
      position: coordinates,
      map,
      draggable: true,
    });

    tempMarker.addListener('drag', () => {
      const newPosition = tempMarker.getPosition();
      document.getElementById('coordenadas').value = `(${newPosition.lat().toFixed(5)}, ${newPosition.lng().toFixed(5)})`;
      setCoordinates(`(${newPosition.lat().toFixed(5)}, ${newPosition.lng().toFixed(5)})`);
      geocoder.geocode({ location: newPosition }, (results, status) => {
        if (status === 'OK' && results[0]) {
          const address = results[0].formatted_address;
          document.getElementById('direccion').value = address;
          setLocationAddress(address);
        } else {
          console.error('Error en obtener la direccion', status)
        }
      })
    });
  }

  return (
    <>
      <form className="formulario-publicacion-terrenos"  onSubmit={navigateToWelcome} >
        <h2>Formulario de Publicación de Terrenos</h2>

        {/* Información Básica */}
        <div className="form-section">
            <h3>Información Básica</h3>
            <div className="input-group">
                <div className="form-field">
                    <label htmlFor="nombreTerreno">Nombre del Terreno</label>
                    <input type="text" id="nombreTerreno" name="nombreTerreno" value={nombreTerreno || ""} onChange={(e) => setNombreTerreno(e.target.value) } required />
                </div>
                <div className="form-field">
                    <label htmlFor="foto">Foto</label>
                    <input type="file" id="foto" name="foto" accept="image/*" />
                </div>
                <div className="form-field">
                    <label htmlFor="direccion">Dirección</label>
                    <input type="text" id="direccion" name="direccion" required value={locationAddress} onChange={(e) => setLocationAddress(e.target.value)} />
                </div>
                <div className="form-field">
                    <label htmlFor="coordenadas">Coordenadas GPS</label>
                    <input type="text" id="coordenadas" name="coordenadas" value={`${coordinates.lat}, ${coordinates.lng}` || ""} required />
                </div>
            </div>
            <div className='form-section'>
              <h3>Ubicacion del Terreno</h3>
              <div id='map' style={{ height: '300px', marginBottom: '20px' }}></div>
            </div>
        </div>

        {/* Descripción del Terreno */}
        <div className="form-section">
          <h3>Descripción del Terreno</h3>
            <div className="input-group">
                <div className="form-field">
                    <label htmlFor="areaTotal">Área Total (m²)</label>
                    <input type="number" id="areaTotal" name="areaTotal" value={areaTotal || ""} onChange={(e) => setAreaTotal(e.target.value)} required />
                </div>
                <div className="form-field">
                    <label htmlFor="servicios">Servicios con los que cuenta</label>
                    <input type="text" id="servicios" name="servicios" value={servicios || ""} onChange={(e) => setServicios(e.target.value)} required />
                </div>
            </div>
          <label htmlFor="descripcion">Breve descripción del terreno</label>
          <textarea id="descripcion" name="descripcion" rows="4" value={descripcion || ""} onChange={(e) => setDescripcion(e.target.value)} required></textarea>
        </div>

        {/* Documentación */}
        <div className="form-section">
          <h3>Documentación</h3>
          <label className="checkbox-label"><input type="checkbox" name="escrituras" checked={escrituras} onChange={handleEscriturasChange} /> Escrituras</label>
          <label className="checkbox-label"><input type="checkbox" name="tituloPropiedad" checked={tituloPropiedad} onChange={handleTituloPropiedadChange} /> Título de Propiedad</label>
        </div>

        {/* Precio */}
        <div className="form-section">
          <h3>Precio</h3>
          <label htmlFor="valorTerreno">Valor Actual del Terreno</label>
          <input type="number" id="valorTerreno" value={valorTerreno || ""} name="valorTerreno" onChange={(e) => setValorTerreno(e.target.value)} required />
        </div>

        {/* Notas Adicionales */}
        <div className="form-section">
          <h3>Notas Adicionales</h3>
          <label htmlFor="observaciones">Observaciones o Comentarios</label>
          <textarea id="observaciones" name="observaciones" value={observaciones || ""} onChange={(e) => setObservaciones(e.target.value)} rows="4"></textarea>
        </div>

        <button type="submit" onClick={handleFormSubmit}>Guardar Terreno</button>
      </form>
      

      {/* Mostrar la alerta si existe */}
      {alerta && (
        <Alert
          titulo={alerta.titulo}
          mensaje={alerta.mensaje}
          tipo={alerta.tipo}
          onClose={cerrarAlerta}
        />
      )}
    </>
  );
};

export default EditarTerreno;
