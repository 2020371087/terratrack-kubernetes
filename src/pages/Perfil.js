import React, { useEffect, useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faL, faUser } from '@fortawesome/free-solid-svg-icons';
import '../styles/Perfil.css';
import { app } from '../api/firebaseConfig';
import { getAuth, onAuthStateChanged, updateEmail} from 'firebase/auth'
import { getFirestore, getDoc, doc, updateDoc } from 'firebase/firestore';


const Perfil = () => {

  const [isEditing, setIsEditing] = useState(false);
  const [editedUser, setEditedUser] = useState({});
  const [email, setEmail] = useState('');
  const [nombre, setNombre] = useState('');
  const [apellidos, setApellidos] = useState('');
  const [telefono, setTelefono] = useState('');
  const [uid, setUID] = useState('');

  const [loading, setLoading] = useState(true)

  const auth = getAuth(app);
  const db = getFirestore(app);

  useEffect(() => {

    const unsubscribe = onAuthStateChanged(auth, async (user) => {
      try {
        if (user) {
          setUID(user.uid)
          setEmail(user.email);

          const docRef = doc(db, 'users', user.uid);
          const docSnap = await getDoc(docRef);

          if (docSnap.exists()) {
            setNombre(docSnap.data().nombre);
            setApellidos(docSnap.data().apellidos);
            setTelefono(docSnap.data().telefono);
          } else {
            alert('Ocurrio un error al obtener los datos')
            window.location.href = '/terratrak/Welcome'
          }
        } else {
          console.log('No se encontró un usuario logueado');
        }
      } catch (error) {
        console.error('Error al obtener datos del usuario:', error);
      } finally {
        setLoading(false);
      }
    });

    return () => unsubscribe();
  }, [])

  const handleEdit = () => {
    setIsEditing(true);
  };

  const handleSave = async () => {
    // Lógica para guardar los cambios, por ejemplo, enviar una solicitud a la API
    
    updateEmail(auth.currentUser, email).then(() => {
      console.log('Correo actualizado!')
    }).catch((error) => console.error(error));
    const usuarioRef = doc(db, 'users', uid);

    const userData = {
      nombre: nombre,
      apellidos: apellidos,
      telefono: telefono,
      uid: uid
    }

    await updateDoc(usuarioRef, userData);

    console.log(editedUser);
    setIsEditing(false);
  };

  const handleChange = (e) => {
    setEditedUser({ ...editedUser, [e.target.name]: e.target.value });
  };

  if (loading) {
    return <p>Cargando...</p>
  }

  return (
    <div className="user-profile-container">
      <h2>Perfil de Usuario</h2>
      <div className="user-details">
        <div className="profile-icon">
          <FontAwesomeIcon icon={faUser} size="5x" />
        </div>
        <div>
          <strong>Nombre:</strong>{' '}
          {isEditing ? (
            <input
              type="text"
              name="name"
              value={`${nombre}` || ''}
              onChange={(e) => setNombre(e.target.value)}
            />
          ) : (
            `${nombre}`
          )}
        </div>
        <div>
          <strong>Apellidos:</strong>{' '}
          {isEditing ? (
            <input
              type="text"
              name="apellidos"
              value={`${apellidos}` || ''}
              onChange={(e) => setApellidos(e.target.value)}
            />
          ) : (
            `${apellidos}`
          )}
        </div>
        <div>
          <strong>Correo:</strong>{' '}
          {isEditing ? (
            <input
              type="text"
              name="email"
              readonly="true"
              value={email || ''}
              style={{background:'#e0e0e0'}}
              onChange={(e) => setEmail(e.target.value)}
            />
          ) : (
            email
          )}
        </div>
        <div>
          <strong>Teléfono:</strong>{' '}
          {isEditing ? (
            <input
              type="text"
              name="phone"
              value={telefono || ''}
              onChange={(e) => setTelefono(e.target.value)}
            />
          ) : (
            telefono
          )}
        </div>
      </div>
      <div className="button-container">
        {isEditing ? (
          <button onClick={handleSave}>Guardar</button>
        ) : (
          <button onClick={handleEdit}>Editar</button>
        )}
      </div>
    </div>
  );
};

export default Perfil;
