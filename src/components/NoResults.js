import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSadTear } from '@fortawesome/free-solid-svg-icons';

const NoResults = ({ message }) => {
  return (
    <div style={{ textAlign: 'center', padding: '20px', color: '#fa6900' }}>
      <FontAwesomeIcon icon={faSadTear} style={{ fontSize: '60px', marginBottom: '20px' }} />
      <p style={{ fontSize: '24px', fontWeight: 'bold' }}>{message}</p>
    </div>
  );
};

export default NoResults;
