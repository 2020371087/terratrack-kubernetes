// Menu.js
import React from 'react';
import '../styles/Menu.css';
import { FaTimes } from 'react-icons/fa';

const Menu = ({ isOpen, onClose }) => {
  return (
    <div className={`menu ${isOpen ? 'open' : ''}`} onClick={onClose}>
        <FaTimes className="close-icon" onClick={onClose} />
      <ul>
        <li>
          <a className='color-link' href="/">Conócenos</a>
        </li>
      </ul>
      <ul>
        <li>
          <a className='color-link' href="/IniciarSesion">Iniciar de sesión</a>
        </li>
      </ul>
      <ul>
        <li>
          <a className='color-link' href="/Registro">Registrarse</a>
        </li>
      </ul>
    </div>
  );
};

export default Menu;
