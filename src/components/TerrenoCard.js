import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHeart as heartSolid } from '@fortawesome/free-solid-svg-icons';
import { faHeart as heartRegular } from '@fortawesome/free-regular-svg-icons';
import { faWhatsapp } from '@fortawesome/free-brands-svg-icons';
import '../styles/TerrenoCard.css';
import { doc, getDoc, getFirestore } from 'firebase/firestore';
import { app } from '../api/firebaseConfig';

const TerrenoCard = ({ nombreTerreno, urlFoto, precioTerreno, descripcion, user_uid, user }) => {
  const [favorito, setFavorito] = useState(false);
  const [telefono, setTelefono] = useState('');

  const handleFavoritoClick = () => {
    setFavorito(!favorito);
  };

  const fetchVendedor = async () => {
    try {
      const db = getFirestore(app);
      const docRef = doc(db, 'users', user_uid);
      const docSnap = await getDoc(docRef);

      if (docSnap.exists()) {
        setTelefono(docSnap.data().telefono);
      } else {
        console.log('No se encontró al vendedor')
      }
    } catch (error) {
      console.error('Error al obtener los datos del vendedor')
    }
  }

  useEffect(() => {
    fetchVendedor();
  }, [])

  const handleWhatsappClick = () => {
    const url = `https://api.whatsapp.com/send?phone=${telefono}`;
    window.open(url, '_blank');
  };

  return (
    <div className="terreno-card">
       <div className="titulo-y-favorito">
          <h2>{nombreTerreno}</h2>
          {/* <button onClick={handleFavoritoClick} className="favorito">
            <FontAwesomeIcon icon={favorito ? heartSolid : heartRegular} />
          </button> */}
        </div>
      {/* <Link to={`/DetalleTerreno`} className="terreno-info"> */}
      <div className='terreno-info'>
        <img src={urlFoto} alt={`Foto de ${nombreTerreno}`} />
        <p>Precio: ${precioTerreno}</p>
        <p>{descripcion}</p>
        </div>
      {/* </Link> */}
      <div className="ver-mas-container">
        <div className='ver-mas-link' >
          {/* <Link to={`/DetalleTerreno`} className="ver-mas-link"> */}
            <button className="ver-mas">Ver más</button>
            </div>
            <div className='ver-mas-link' >
            {/* <Link to={`/ComprarTerreno/${nombreTerreno}`} className="comprar">
              Comprar
            </Link> */}
            </div>
            <div className='ver-mas-link'>
          {user && <button className="whats" onClick={handleWhatsappClick}>
            <FontAwesomeIcon icon={faWhatsapp} />  WhatsApp
          </button>}
        </div>
        </div>
    </div>
  );
};

export default TerrenoCard;

