import React, { useState, useEffect } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheckCircle, faTimesCircle } from '@fortawesome/free-solid-svg-icons';
import '../styles/Alert.css';

const Alert = ({ titulo, mensaje, tipo, onClose }) => {
  const [visible, setVisible] = useState(true);

  useEffect(() => {
    const timer = setTimeout(() => {
      setVisible(false);
      onClose();
    }, 7000);

    return () => clearTimeout(timer);
  }, [onClose]);

  const getIcon = () => {
    if (tipo === 'success') {
      return <FontAwesomeIcon icon={faCheckCircle} className="icon-success" color="#4CAF50" />;
    } else if (tipo === 'error') {
      return <FontAwesomeIcon icon={faTimesCircle} className="icon-error" color="#f44336" />;
    } else {
      return null;
    }
  };

  return (
    <div className={`modal ${visible ? 'visible' : 'invisible'}`}>
      <div className={`alert ${tipo === 'success' ? 'alert-success' : 'alert-error'}`}>
        <button className="close-button" onClick={() => {setVisible(false); onClose();}}>
          X
        </button>
        <div className="icon-container">
          {getIcon()}
        </div>
        <div className="content">
          <div className="title">
            {tipo === 'success' ? 'Éxito' : 'Error'}
          </div>
          <div className="message">
            <p>{mensaje}</p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Alert;
