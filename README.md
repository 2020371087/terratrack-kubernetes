
# Terratrack

## Descripción
TerraTrack es una aplicación diseñada para facilitar el proceso de compra y venta de terrenos. Esta plataforma digital proporciona a los usuarios una amplia gama de herramientas y recursos que permiten tanto a compradores como a vendedores explorar y  llevar a cabo compras de manera efectiva.

## Índice
1. [Instalación](#instalación)
2. [Funcionalidades](#funcionalidades)
3. [Uso](#uso)
4. [Contribuciones](#contribuciones)
5. [Licencia](#licencia)

## Instalación
Asegúrate de tener los siguientes requisitos antes de comenzar la instalación.

- **Node.js**

Para instalar este proyecto, sigue estos pasos:

1. Clona el repositorio: `git clone https://gitlab.com/uteq8791439/notas-app.git`
2. Abre la carpeta del proyecto: `cd terratrack`
3. Instala las dependencias: `npm install`
4. Inicia la aplicación: `npm start`
5. Puedes acceder a la aplicación en tu navegador: `http://localhost:3000`

## Funcionalidades
- **Registro:** Permite la creación de usuarios nuevos para poder acceder a todas las funcionalidades de la aplicación.
- **Login:** Una vez que se validan las credenciales, permite el acceso a la aplicación y todas las funcionalidades.
- **Perfil:** Muestra toda la información personal que registró el usuario.
- **Editar Perfil:** Permite modificar la información personal registrada por el usuario.
- **Ver mis terrenos:** Permite al usuario ver los terrenos que ha publicado.
- **Barra de búsqueda:** Permite al usuario buscar terrenos por nombre, lugar o precio.
- **Editar terreno:** Modifica la información de un terreno.
- **Publicar terreno:** Permite agregar un terreno nuevo que se enlazará al usuario.

## Uso
Para aprovechar al máximo TerraTrack, sigue estos sencillos pasos:

1. Abre la aplicación en tu navegador: `localhost:3000`

2. **Registro de Usuario:**
   - Haz clic en "Registrarse" y completa el formulario para crear tu cuenta.
   - Proporciona la información necesaria, como nombre, teléfono, correo electrónico y contraseña.

3. **Inicio de Sesión:**
   - Después de registrarte, inicia sesión con tu correo y contraseña.

4. **Explorar Terrenos:**
   - Utiliza la barra de búsqueda para encontrar terrenos por nombre, lugar o precio.
   - Explora la variedad de terrenos disponibles para la compra.

5. **Ver tus Terrenos:**
   - Accede a la sección "Mis Terrenos" para ver la lista de terrenos que has publicado.

6. **Editar Información Personal:**
   - Dirígete a tu perfil para editar la información personal registrada.

7. **Publicar un Nuevo Terreno:**
   - Agrega un nuevo terreno a la plataforma haciendo clic en "Publicar terreno".
   - Completa los detalles del terreno, como nombre, ubicación y precio.

8. **Modificar Información de un Terreno:**
   - Edita la información de un terreno existente seleccionando la opción "Editar terreno".

Recuerda guardar los cambios cuando modifiques información y siempre cierra sesión al terminar de usar la aplicación.

## Contribuciones

Agradecemos cualquier contribución que desees realizar a nuestro proyecto. Si deseas participar, aquí tienes algunas formas en las que puedes ayudar:

- **Desarrollo de Código:** Si eres un desarrollador, puedes colaborar en la mejora de la aplicación, implementar nuevas características o solucionar problemas existentes. Simplemente sigue estos pasos:

    1. Haz una copia del proyecto.
    2. Crea una rama para tu contribución (`git checkout -b feature/nueva-caracteristica`).
    3. Realiza tus cambios y haz commit (`git commit -m 'Añade nueva característica'`).
    4. Haz push a la rama (`git push origin feature/nueva-caracteristica`).
    5. Abre un pull request.

- **Reporte de Problemas:** Si encuentras algún error o tienes una idea para mejorar la aplicación, no dudes en informarnos a travez de un correo a *contact-project+2020371018-terratrack-51842488-issue-@incoming.gitlab.com* Proporciona detalles completos para que podamos abordarlos eficazmente.

- **Documentación:** La documentación clara y precisa es esencial. Puedes contribuir escribiendo o mejorando la documentación de usuario o desarrollador para hacer que el proyecto sea más accesible.

- **Pruebas y Comentarios:** Probar la aplicación y proporcionar retroalimentación sobre tu experiencia es valioso. Comparte tus comentarios y sugerencias para ayudarnos a mejorar.

## Licencia
- MIT (Massachusetts Institute of Technology).
